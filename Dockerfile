FROM python:3.8.3-alpine3.12

ENV APP_DIR /app

# install basic packages
RUN apk add --no-cache --update wget

# download mwa release from github and unpack
RUN wget -O /mwa2-1.1.tgz https://github.com/paulsuh/mwa2/archive/1.1.tar.gz
RUN mkdir /app_tmp
RUN tar zxf /mwa2-1.1.tgz -C /app_tmp
RUN mv /app_tmp/mwa2-1.1 /app

#clean up
RUN rmdir /app_tmp
RUN rm /mwa2-1.1.tgz

# install required python packages
RUN pip install -r /app/requirements.txt

# create user to run the web app
RUN addgroup -g 201 gunicorn
RUN adduser -u 201 -D -H -G gunicorn -s /sbin/nologin -g "Gunicorn User" gunicorn 

# Install munki from source into /usr/local/munki to get makecatalogs
RUN wget -O /munki-5.0.0.tgz https://github.com/munki/munki/archive/v5.0.0.tar.gz
RUN mkdir -p /usr/local
RUN tar -zxf /munki-5.0.0.tgz -C /usr/local
RUN mv /usr/local/munki-5.0.0 /usr/local/munki
RUN rm /munki-5.0.0.tgz

COPY docker/docker_entrypoint.sh /
COPY docker/django_setup.sh /
COPY docker/settings.py /app/munkiwebadmin/

WORKDIR /app

ENTRYPOINT ["/bin/sh", "/docker_entrypoint.sh"]

# NOTE: This runs the app in development mode
# Use gunicorn for production use
CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]
