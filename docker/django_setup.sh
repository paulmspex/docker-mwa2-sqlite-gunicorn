#!/bin/sh

# Django setup steps that need to be run as the gunicorn user

python manage.py migrate --noinput
python manage.py collectstatic --clear --noinput
python manage.py shell -c "from django.contrib.auth.models import User; User.objects.create_superuser('admin', 'admin@example.com', '${ADMIN_PASS_ACTUAL}')"
