#!/bin/bash

export ADMIN_PASS_ACTUAL="${ADMIN_PASS:-password}"
APP_DIR_ACTUAL="${APP_DIR:-/app}"
MUNKI_REPO_ACTUAL="${MUNKI_REPO:-/munki-repo}"

cd $APP_DIR_ACTUAL

# create volume directories
for ONE_DIR in /munki/munki_repo/pkgs /munki/munki_repo/pkgsinfo /munki/munki_repo/icons \
		/munki/munki_repo/catalogs /munki/munki_repo/manifests /munki/mwa2/db /munki/mwa2/static; do
	if [ ! -d "${ONE_DIR}" ]; then
		/bin/mkdir -p "${ONE_DIR}"
	fi
done

chown -R gunicorn /munki/munki_repo
chown -R gunicorn /munki/mwa2

# create secret key for the app
python manage.py create_secret_key

su gunicorn -s /bin/sh -c '/bin/sh /django_setup.sh'

exec "$@"
