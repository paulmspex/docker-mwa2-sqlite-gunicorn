# MWA2 Docker Container

Part of a container set to properly deploy MunkiWebAdmin2 with a real app server driving it and automatic setup of the database. This is meant to be deployed in conjunction with an Nginx front end container, via docker-compose. 

The original was actually broken and had a lot of junk. Very little survives from the first one. 

- MWA2 is now downloaded from a release tag as a .tar.gz to avoid needing git
- Full munki including libs is downloaded from Github so that makecatalogs will actually work
- Entrypoint script sets up a superuser account with password from environment variables passed in
- Entrypoint script also generates a unique secret key

